### Ovirt/Vdsm packaging

Since oVirt team provides fully functional and stable packages for EL6/CentOS 6/etc., I stopped support my own packages.

If you want to upgrade from my 3.2 to stable 3.3, you can use this [oVirt 3.2 -> 3.3.0 Update HOW-TO](/ovirt_rpm/start33)


  - VDSM 4.9/4.10.x and oVirt Engine 3.0/3.1/3.2 RPM's for CentOs/Scientific/Oracle Linux 6.4
    - [oVirt 3.2 -> 3.3.0 Update HOW-TO](/ovirt_rpm/start33) - Oct 28 2013 upgrade from my 3.2 to official 3.3
    - [oVirt 3.2 latest git builds](/ovirt_rpm/start) - updated Dec 14 2012 (oVirt,Vdsm, oVirtReports)
    - [oVirt 3.2.2 stable](/ovirt_rpm/start32) - updated Jun 07 2013 (Vdsm,oVirt)
    - [oVirt 3.1 stable](/ovirt_rpm/start31) - updated Mar 11 2013 (Vdsm)
    - [VMs control](/ovirt_rpm/vmcontrol) (oVirt/REHEV agent, Spice)
    - [Troubleshooting](/ovirt_rpm/troubleshooting)
    - [Repositories](/ovirt_rpm/repos)
    - [Tips and a little howtos](/ovirt_rpm/howto)
    - [Links](/ovirt_rpm/links)
  - VDSM hooks
    - [Simple qemu](/vdsm_hooks/simpleqemu)
    - [Simple nwfilter](/vdsm_hooks/simplenwfilter)
  - OpenShift RPM's for CentOS 6.4
    - [OpenShift 1.1 builded from RedHat SRPM](/oshift_rpm/start)
    - [Links](/oshift_rpm/links)