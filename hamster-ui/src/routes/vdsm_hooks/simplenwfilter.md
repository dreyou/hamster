<script>
    import Note from '$lib/Note.svelte';
</script>
FIXME


## Simple Network Filter

Load filter xml rules file from specified server while Vm start, then create and attach
 filter to virtual machine instance.

More info about libvirt's network filters http://libvirt.org/formatnwfilter.html

### Usage

Define vm custom property:

````text
snfilter=192.168.1.1:8080/filters,filtername
````

__192.168.1.1:8080/filters__ - url of http server which holds xml nwfilter files, 

__filtername__ - name of the filter file without "xml" extension.

Example content of filtername.xml

````xml
<filter name='filtername'>
  <filterref filter='clean-traffic'/>
  <rule action='accept' direction='in'>
      <tcp dstportstart='22'/>
  </rule>
  <rule action='accept' direction='in'>
      <tcp dstportstart='80'/>
   </rule>
  <rule action='accept' direction='out'>
      <tcp dstportstart='22'/>
  </rule>
  <rule action='accept' direction='out'>
      <tcp dstportstart='80'/>
  </rule>
  <rule action='accept' direction='out'>
      <icmp/>
  </rule>
  <rule action='accept' direction='out'>
      <udp dstportstart='53'/>
  </rule>
  <rule action='drop' direction='inout'>
      <all/>
   </rule>
</filter>
````

<Note>

**filter name='filtername'** - must be same as filename i.e. if file name **filtername.xml**
 then filter name must be **filtername**

</Note>
