<script>
    import Note from '$lib/Note.svelte';
</script>
## samplepdf - Sample spring-boot XML to FO to PDF project

Sample spring-boot application to demonstrate Apache FOP engine usage.

Can be used to transform xml files to fo files using xlst
template and output fo files via Apache FOP to pdf or png files.

<Note warning=true>
Old CI/CD procedure under review and will be changed.
</Note>

[GitHub repository](https://github.com/dreyou/samplepdf) ->

-> **Temporary disabled** - [Jenkins build server](https://jenkins.dreyou.org:22443/) ->

-> [Jar artifacts repository](http://www.dreyou.org/pub/misc/spring-boot/) ->

-> **Temporary disabled** - [http://samplepdf.dreyou.org](http://samplepdf.dreyou.org)
Amazon Elastic Beanstalk Docker Container with application (start on demand)