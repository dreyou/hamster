<script>
    import Note from '$lib/Note.svelte';
</script>
## Hamster - Sample Svelte/Kit project

Sample project to demonstrate [Svelte/Kit](https://kit.svelte.dev/) usage and manage this site.

Can be used as a sample Svelte/Kit web site with basic template view and markdown powered pages (i.e. simple WIKI like static site).

CI/CD procedure powered by GitLab (check .gitlab-ci.yml).

<Note>
More information about CI/CD pipelines will be added 
</Note>

[GitLab repository](https://gitlab.com/dreyou/hamster)

