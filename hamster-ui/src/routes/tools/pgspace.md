## pgspace - sample Vagrant project

Vagrant project with some shell scripts to create a Spacewalk instance with hot-standby Postgresql backends.

[Github link](https://github.com/dreyou/pgspace)