## spwd - Sample golang /proc to Web Daemon

Sample golang /proc filesystem parser.

Can be used like "top", but from browser to control system load, etc.
Also can be used to send system load data to elacticsearch instance.

[GitHub repository](https://github.com/dreyou/spwd) -> [Jenkins build server](https://jenkins.dreyou.org:22443) -> [RPMS repository](http://www.dreyou.org/pub/rhel/6/x86_64/), [SRPMS repository](http://www.dreyou.org/pub/rhel/6/SRPMS/)

To use it, install local repository [Local repository](/tools/repo) then run yum install command:

````bash
yum install spwd
service spwd start
````

Now you can access to daemon - http://hostname:4000. By befault access limited only to 127.0.0.1

To configure, edit config file
````bash
vim /opt/spwd/spwd.gcfg
````
then restart
````bash
service spwd restart
`````
Config file example

````ini
;Defaulf config
[Main]
;Update statictics data in miliiseconds
UpdateInterval = 5000
;Send statictics data in seconds
SendInterval = 60
Listen = localhost:4000
Allow = 127.0.0.1
[Js]
MaxPoints = 50
;Time to refresh page, min
TimeToRefresh = 10
;Time to reload data, sec
TimeToReload = 2
[Elasticsearch]
Url = http://localhost:9200
;Additional host id
HostId = default_host_id
;Send main data to elasticsearch index spwd with type proc
Send = false
;Send processes data to elasticsearch index spwd with type processes
SendProcesses = false
;Process data will be sended to elastiicsearch when process processor load (%) > LoadTreshold
LoadTreshold = 0.1
;or process memory usage (%) > MemTreshold
MemTreshold = 0.1
````