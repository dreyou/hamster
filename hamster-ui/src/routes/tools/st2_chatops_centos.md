## st2_chatops - sample StackStorm&ChatOps Provisioning script

Vagrant/Shell script project to deploy StackStorm with Hubout and Ansible integration into CentOs 7.1 Linux.

[Github link to provisioning scripts](https://github.com/dreyou/st2_chatops_centos)

[Github link to small activemq queues control pack](https://github.com/dreyou/st2-chatops-misc)