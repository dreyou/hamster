## ceph_sample - sample Ceph Provisioning script

Vagrant/Shell script project to deploy Ceph cluster with rbd and Ceph Filesystem.

[Github link to provisioning scripts](https://github.com/dreyou/ceph_sample)