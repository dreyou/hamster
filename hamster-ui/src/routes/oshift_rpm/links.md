## Useful links for OpenShift

  * [OpenShift Main Site](http://www.openshift.com/) 
  * [RedHat OpenShift Enterprise Documentation](https://access.redhat.com/site/documentation/OpenShift_Enterprise/)
  * [RedHat OpenShift Enterprise srpms](ftp://ftp.redhat.com/redhat/linux/enterprise/6Server/en/RHOSE/SRPMS/)