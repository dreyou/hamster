## OpenShift RPM's for CentOs builded from ReadHat OpenShift SRPM

Version 1.1

### Installation instruction

Based on "[Build your own PaaS](https://www.openshift.com/wiki/build-your-own)" - wiki article
 and  [OpenShift Enterprise Deployment Guide](https://access.redhat.com/site/documentation/en-US/OpenShift_Enterprise/1/html/Deployment_Guide/)


### Repositories

[openshift-dre.repo](http://www.dreyou.org/oshift/openshift-dre.repo)

````ini
[openshift-dre]
name=OpenShift Dre Packages
baseurl=http://www.dreyou.org/oshift/repo/
http://www1.dreyou.org/oshift/repo/
enabled=1
gpgcheck=0

[openshift-dre-misc]
name=OpenShift Dre Misc
baseurl=http://www.dreyou.org/oshift/misc/
http://www1.dreyou.org/oshift/misc/
enabled=1
gpgcheck=0
````

...

coming soon

...