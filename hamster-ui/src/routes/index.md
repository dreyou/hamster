<script>
  let name = 'Andrey Gordeev';
</script>

## About
  This is **{name}** personal home page

## Current interests
### Tools&Samples
  - [Svelte/Kit](/tools/hamster) - sample project (this site) 
  - [Local repository](/tools/repo) - installation
  - [spwd](/tools/spwd) - sample golang project
  - [samplepdf](/tools/samplepdf) - sample spring-boot project
  - [ebstarter](/tools/ebstarter)- sample python project
  - [pgspace](/tools/pgspace) - sample Vagrant project
  - [st2_chatops_centos](/tools/st2_chatops_centos)- sample StackStorm&ChatOps Provisioning scripts for CentOs 7
  - [ceph_sample](/tools/ceph_sample)- sample Ceph Provisioning scripts for CentOs 6
  - [github](https://github.com/dreyou) - little bit more samples, playgrounds etc.

### Archive
  - [oVirt 3.x/VDSM 4.x/OpenShift 1.1](/archive) - old packages, documentaion, etc.

### Contacts

e-mail: dreyou☉gmail.com