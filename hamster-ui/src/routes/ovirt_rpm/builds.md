## Builds information

Latest build - **Dec 14 2012** (buid-33)

### Engine notes:

This build is corresponds to oVirt 3.2 Alpha version.

v 3.2.0-1(.33), builded from git - 3d71f6b97cb9b6799876ec6b3ceebd0d21a36b06

Changes from original packages:

  * corrected ovirt-engine-dwh startup script to using ssl keystore with oVirt CA certificate
  * add jasperreports-server-pro builded from rhev rpms
  * add ovirt-report-3.1 package builded from rhev-report rpms
  * replace pc-0.14 with pc for EmulatedMachine
  * added local build number to rpm release info
  * ovirt-engine-jbossas replaced with ovirt-engine-jbossas711 (rpm builded from full binary JbossAS 7.1.1.Final distribution)
  * added missing requirement for apr packge
  * all external java libs dependencies removed
  * all links to java libs replaced to corresponding JbossAs 7.1.1 libs
  * install sistem V service startup script, instead systemd scrypt

patches:

  * http://www.dreyou.org/ovirt/engine.patch
  * http://www.dreyou.org/ovirt/cli.patch
  * http://www.dreyou.org/ovirt/sdk.patch
  * http://www.dreyou.org/ovirt/node.patch
  * http://www.dreyou.org/ovirt/dwh.patch