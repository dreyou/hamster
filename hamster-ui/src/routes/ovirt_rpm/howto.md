## Linux Template Creation
FIXME

Before creating template.

Clean host ssh keys

````shell script
rm -f /etc/ssh/*key*
````

Clean root password and force password change:

````shell script
passwd -d root
chage -d 0 root
````

To remove the reference to eth0 remove or move out file _/etc/udev/rules.d/70-persistent-net.rules_
  mv /etc/udev/rules.d/70-persistent-net.rules /etc/udev/rules.d/70-persistent-net.rules.old

## Connect to engine via oVirt shell

Install (if not installed)

````shell script
yum install ovirt-engine-cli
````

Connect

````shell script
ovirt-shell -c -I -l https://localhost/api -u admin@internal
````
  
List hosts

````shell script
list hosts
````  

## Adding ActiveDirectory domain

Add:

````shell script
engine-manage-domains  -provider=ActiveDirectory -action=add -domain='domain.local' -user='admin' -interactive
````
  
## Setting engine config values

Set:

````shell script
engine-config -s UserDefinedVMProperties='hostusb=[\w:&]+;snfilter=^.+$' --cver=3.1
````

Get:

````shell script
engine-config -g UserDefinedVMProperties
````

## Virt-v2v

Import existing kvm vms into oVirt. 

To install (version 0.8.3-5 resides in standart repositories):

````shell script
yum install virt-v2v
````

example:

````shell script
virt-v2v -b ovirtmgmt -i libvirtxml -o rhev -os nfs.domain.local:/ovirt/export vm.xml
````
   
nfs.domain.local:/ovirt/export - preconfigured oVirt export domain

vm.xml - libvirt vm xml description