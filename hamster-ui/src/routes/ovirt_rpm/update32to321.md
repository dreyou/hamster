## oVirt 3.2 to 3.2.1 Update Procedure

To update ovirt 3.2 to version 3.2.1 you can follow next procedure:

If possible, [backup engine](http://www.ovirt.org/Backup_And_Restore_Engine), also you can backup all VM to export domain using oVirt console.


On oVirt engine node execute **engine-upgrade**

````shell script
  engine-upgrade
````

If **engine-upgrade** found new version of **ovirt-engine-setup** you will be prompted to update this package, do it, then run **engine-upgrade** again:

````shell script
  yum update ovirt-engine-setup
  
  engine-upgrade
````
  
engine-upgrade will stop oVirt engine, then back up database, then update packages and then start oVirt engine again.
