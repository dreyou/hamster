## Useful links for oVirt/RHEV/Vdsm and more

oVirt
  * [oVirt main site](http://www.ovirt.org)
  * [Direct link to oVirt repositories](http://www.ovirt.org/releases/)
  * [Official oVirt CentOS repositories](http://wiki.centos.org/HowTos/oVirt)

oVirt/Vdsm development
  * [oVirt/Vdsm Gerrit](http://gerrit.ovirt.org/)
  * [RedHat oVirt/Vdsm Bugzilla](https://bugzilla.redhat.com/page.cgi?id=browse.html&product=oVirt&bug_status=open&tab=summary)
  * Version 3.1
    * [Vdsm Gerrit, branch ovirt-3.1](http://gerrit.ovirt.org/#/q/project:vdsm+branch:ovirt-3.1,n,z)
    * [oVirt Gerrit, branch engine_3.1](http://gerrit.ovirt.org/#/q/project:ovirt-engine+branch:engine_3.1,n,z)
  * Version 3.2
    * [Vdsm Gerrit, branch ovirt-3.2](http://gerrit.ovirt.org/#/q/project:vdsm+branch:ovirt-3.2,n,z)
    * [oVirt Gerrit, branch engine_3.2](http://gerrit.ovirt.org/#/q/project:ovirt-engine+branch:engine_3.2,n,z)

oVirt-Spice-Google Chrome
    * http://lists.ovirt.org/pipermail/users/2012-December/010925.html

Gluster
    * http://bits.gluster.org/pub/gluster/glusterfs/3.4.0alpha/ - latest Gluster required by oVirt 3.2

RHEV
  * [RedHat RHEV Documentation](http://docs.redhat.com/docs/en-US/Red_Hat_Enterprise_Virtualization/index.html)
  * [RedHat RHEV srpms](ftp://ftp.redhat.com/redhat/linux/enterprise/6Server/en/RHEV/SRPMS/)

Misc
  * [Jpackage (Java packages) site](http://www.jpackage.org/)
  * [Jpackage (Java packages) repository](ftp://mirrors.dotsrc.org/jpackage/6.0/generic/free/RPMS/)