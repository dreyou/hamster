## oVirt 3.1 Update Procedure

To update ovirt 3.1 you can follow next procedure:

1. If possible, in oVirt console make all VM backup to export domain.

2. In oVirt node update execute **engine-upgrade**

````shell script
engine-upgrade
````

3. If **engine-upgrade** find new version of **ovirt-engine-setup** you will be prompted to update this package, do it, then run **engine-upgrade** again:

````shell script
yum update ovirt-engine-setup
engine-upgrade
````
  
4. engine-upgrade will stop oVirt engine, then back up database, then update packages and then start oVirt engine again.
