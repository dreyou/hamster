## Build information

Latest build - **Aug 15 2012** (buid-19)

### Engine notes:

v 3.1.0-3 (.19), builded from git - b81129ea2b3bbddf949599ff34b84f7e7c8e80bc (branch engine_3.1)
  * replace pc-0.14 with pc for EmulatedMachine
  * added missing requirement for _apr_ packge
  * added local build number to rpm release info
  * ovirt-engine-jbossas replaced with ovirt-engine-jbossas711 (rpm builded from full binary JbossAS 7.1.1.Final distribution)
  * all external java libs dependencies removed
  * all links to java libs replaced to corresponding JbossAs 7.1.1 libs
  * install sistem V service startup script, instead systemd scrypt

patches:

  * http://www.dreyou.org/ovirt/engine31.patch
  * http://www.dreyou.org/ovirt/cli.patch
  * http://www.dreyou.org/ovirt/sdk.patch
  * http://www.dreyou.org/ovirt/node.patch
  * http://www.dreyou.org/ovirt/dwh.patch