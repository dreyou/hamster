<script>
    import Note from '$lib/Note.svelte';
</script>
## VDSM 4.10.x and oVirt Engine 3.2 RPM's for CentOs/Scientific Linux 6.3


Latest git builds:

  * Vdsm  - **Dec 11 2012** (buid-26) ([more info](/ovirt_rpm/buildsvdsm))
  * oVirt (3.2 Alpha)  - **Dec 14 2012** (buid-34) ([more info](/ovirt_rpm/builds))

<Note header="About builds">

Before placing new build I try to make minimal install check, i.e.:
  - install engine and vdsm
  - add hosts
  - add NFS storage domain
  - add ISO domain
  - create VM, migrate VM
  
</Note>

## Installation tips


### Adding repo to nodes and management server
````bash
  sudo wget http://www.dreyou.org/ovirt/ovirt-dre.repo -P /etc/yum.repos.d/
````

To install latest oVirt version disable **ovirt-dre31**, **vdsm-dre31** repos
 (_enabled=0_) and enable **ovirt-dre**, **vdsm-dre** repos (_enable=1_) in /etc/yum.repos.d/ovirt-dre.repo 


Direct links to repositories:

  * http://www.dreyou.org/ovirt/
  * http://www1.dreyou.org/ovirt/


### Node preparation

Add epel repositories for __gluster__ (needed by vdsm-gluster) and for __python-pthreading__ (http://fedoraproject.org/wiki/EPEL/FAQ).

````bash
  yum clean all
  yum makecache
````
  
Check that the engine FQDN is resolvable from the node (For example, edit /etc/hosts).

Check that the CentOS version is 6.3, make update if needed.

## Install ovirt-engine

### Installing Prerequisite Packages

Now you __must__ have java 1.7 to install and use oVirt.

````bash
  yum install yum install java-1.7.0-openjdk
````

Add epel repositories for python-ply, python-kitchen needed by ovirt-engine-cli (http://fedoraproject.org/wiki/EPEL/FAQ).

### Install ovirt engine

````bash
  yum install ovirt-engine
````

### Configure ovirt engine

````bash
  engine-setup
````
  
Now you can enter to engine console and add you nodes to cluster.

### Troubleshooting

If you have some problems while installing engine or running Vms look [here)[/ovirt_rpm/troubleshooting).