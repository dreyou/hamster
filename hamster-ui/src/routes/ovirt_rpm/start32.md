<script>
    import Note from '$lib/Note.svelte';
</script>
## VDSM 4.10.x and oVirt Engine 3.2.x RPM's for CentOs/Scientific Linux 6.3

[OVirt 3.2 Release Notes](http://www.ovirt.org/OVirt_3.2_release_notes)

Version 3.2.2 - Stable builds

  * Vdsm  - **Jun 07 2013** (buid-23)  ([more info](/ovirt_rpm/buildsvdsm32))
  * oVirt (3.2.2 Stable)  - **Jun 07 2013** (buid-43) ([more info](/ovirt_rpm/builds32))

<Note header="Known issues">

  * All-in-one installation may not work. 
  * GlusterFS may not work. 

</Note>

### Update tips

[oVirt 3.2.1 to 3.2.2 update procedure](/ovirt_rpm/update321to322)

[oVirt 3.2 to 3.2.1 update procedure](/ovirt_rpm/update32to321)

[oVirt 3.1 to 3.2.1 update procedure](/ovirt_rpm/update31to32)

### Installation tips


#### Adding repo to nodes and management server

````shell script
sudo wget http://www.dreyou.org/ovirt/ovirt-dre.repo -P /etc/yum.repos.d/
````

Chek if **ovirt32-dre**, **vdsm32-dre** repositories enabled by default in /etc/yum.repos.d/ovirt-dre.repo (_enable=1_), i.e. :

````ini
[vdsm32-dre]
name=Vdsm for oVirt 3.2 repo
baseurl=http://www.dreyou.org/ovirt/vdsm32/
http://www1.dreyou.org/ovirt/vdsm32/
enabled=1
gpgcheck=0

[ovirt32-dre]
name=oVirt 3.2 engine repo
baseurl=http://www.dreyou.org/ovirt/ovirt-engine32/
http://www1.dreyou.org/ovirt/ovirt-engine32/
enabled=1
gpgcheck=0
````

Then:

````shell script
yum clean all
yum makecache
````

Direct links to repositories:

  * http://www.dreyou.org/ovirt/
  * http://www1.dreyou.org/ovirt/


#### Node preparation

Add epel repositories for __mom__ and for __python-pthreading__ packages (http://fedoraproject.org/wiki/EPEL/FAQ).

````shell script
yum clean all
yum makecache
````

If you plan to use __gluster__ volumes, add gluster 3.4-alpha repositories (needed by vdsm-gluster)

````shell script
sudo wget http://download.gluster.org/pub/gluster/glusterfs/qa-releases/3.4.0alpha2/CentOS/glusterfs-alpha-epel.repo -P /etc/yum.repos.d/
````
  
Check that the engine FQDN is resolvable from the node (For example, edit /etc/hosts).

Check that the CentOS version is 6.4, make update if needed.

### Install ovirt-engine

#### Installing Prerequisite Packages

Now you __must__ have java 1.7 to install and use oVirt.

````shell script
yum install java-1.7.0-openjdk
````

Add epel repositories for python-ply, python-kitchen needed by ovirt-engine-cli (http://fedoraproject.org/wiki/EPEL/FAQ).

#### Install ovirt engine

````shell script
yum install ovirt-engine
````

#### Configure ovirt engine

````shell script
engine-setup
````
  
Now you can enter to engine console and add you nodes to cluster.

#### Install ovirt-reports

Install history database

````shell script
yum install ovirt-engine-dwh
````

Configure ovirt-engine-dwh

````shell script
ovirt-engine-dwh-setup
````

Install reports

````shell script
yum install ovirt-engine-reports
````

Configure reports

````shell script
ovirt-engine-reports-setup
````

#### Troubleshooting

If you have some problems while installing engine or running Vms look [here](/ovirt_rpm/troubleshooting).