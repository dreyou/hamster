## Builds information

Latest build - **Jun 07 2013** (buid-43)

### Engine notes:

This build is corresponds to oVirt 3.2.2

v 3.2.2.1-1 (.43), builded from git - f28fd335e08070bab4351775d2c85710d28e13f9 (tag 3.2.2)

Changes from original packages:

  * corrected ovirt-engine-dwh startup script to using ssl keystore with oVirt CA certificate
  * add jasperreports-server 4.7.0 rpm builded from fedora srpm
  * add rhevm-guest-agent builded from rhevm source rpm
  * replace pc-0.14 with pc for EmulatedMachine
  * added local build number to rpm release info
  * jboss-as-7.1.1 replaced with ovirt-engine-jbossas711 (rpm builded from full binary JbossAS 7.1.1.Final distribution)
  * added missing requirement for apr packge
  * added patches from upstream version:
    * http://gerrit.ovirt.org/#/c/11825/ - Change Idf596fa9: packaging: configuring ovirt-engine on upgrade
    * http://gerrit.ovirt.org/#/c/12076/ - Change Ifc40a2d4: core: Fix db upgrade in PostgreSQL 8.x
    * http://gerrit.ovirt.org/#/c/11382/ - Change I5e827d5b: core: Upgrade from 3.1 to 3.2 fails : upgrade
    * http://gerrit.ovirt.org/#/c/12211/ - Change I57c62cf6: packaging: Updated getEngineVersion functionality
    * http://gerrit.ovirt.org/#/c/9679/ - Change I47f81d8a: [wip] webadmin: enable the spice-xpi extension for chrome
    * http://gerrit.ovirt.org/#/c/13045/ - Cannot confirm Host has been rebooted when having a single host in the system

patches:

  * http://www.dreyou.org/ovirt/engine32.patch
  * http://www.dreyou.org/ovirt/cli.patch
  * http://www.dreyou.org/ovirt/sdk.patch
  * http://www.dreyou.org/ovirt/node.patch
  * http://www.dreyou.org/ovirt/dwh.patch
  * http://www.dreyou.org/ovirt/reports.patch