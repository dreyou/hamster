### Install rhev agent into CentOS 6.x guest VM
_rhev-agent rpm's builded from RedHat src rpm's_

Add repo, same as described in "Installation tips" section.

````shell script
yum -y install rhevm-guest-agent rhevm-guest-agent-pam-module

chkconfig rhev-guest-agentd on
service rhev-guest-agentd start
````

### Install rhev agent into CentOS 5.x guest VM
_rhev-agent rpm's builded from RedHat src rpm's_

````shell script
wget http://www.dreyou.org/ovirt/misc/rhev-agent-2.3.16-3.noarch.rpm
yum -y --nogpgcheck localinstall rhev-agent-2.3.16-3.noarch.rpm

chkconfig rhev-agentd on
service rhev-agentd start
````

### Using SPICE console

You can use **Firefox** with spice-xpi to run spice client from ovirt management console.

````shell script
yum install spice-xpi
````  

Also you can use SPICE console from **Google Chrome**.

Get patched (rpm builded from rhev srpms using [this info](http://lists.ovirt.org/pipermail/users/2012-December/010925.html))
 spice-xpi-2.7-22.el6.i686.rpm or spice-xpi-2.7-22.el6.x86_64.rpm

````shell script
wget http://www.dreyou.org/ovirt/misc/spice-xpi-2.7-22.el6.i686.rpm
````
or
````shell script
wget http://www.dreyou.org/ovirt/misc/spice-xpi-2.7-22.el6.x86_64.rpm
````
  
Then remove standart spice-xpi

````shell script
yum remove spice-xpi
````

And install patched package

````shell script
yum localinstall spice-xpi-2.7-22.el6.i686.rpm
````