## Build information

Latest build - **Dec 11 2012** (buid-26)

### Vdsm notes

v 4.10.2-0.101 (.26), builded from git - f0037346d07e6197c73c62653d806787971d4635

Changes from original packages:

  * added qemu-rhev and sanlock 2.3-4 packages builded from redhat srpms
  * added softdog module autoloading
  * added local build number to rpm release info
  * turn off initctl check for libvirtd
  * move my nonstandart hooks (simpleqemu,snfilter) to separate repo

patches:
  * http://www.dreyou.org/ovirt/vdsm.patch