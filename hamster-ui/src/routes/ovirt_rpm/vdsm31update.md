## Vdsm31 Update Procedure

To update vdsm on each host you can follow next procedure:

1. In oVirt console move host to "Maintenance" mode

2. Update vdsm

````shell script
  yum update vdsm*
````  
  
3. Restart vdsm

````shell script
  service vdsmd restart
````  

4. In oVirt console activate host.