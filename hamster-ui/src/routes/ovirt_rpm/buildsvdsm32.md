## Build information

Latest build - **Jun 07 2013** (buid-23)

**Jun 07 2013** - added new version of qemu-kvm-rhev

### Vdsm notes

v 4.10.3-0.36 (.23), builded from git -  f28fd335e08070bab4351775d2c85710d28e13f9 (branch ovirt-3.2)

Changes from original packages:

  * added qemu-rhev and sanlock 2.3-4 packages builded from redhat srpms
  * added softdog module autoloading
  * added local build number to rpm release info
  * turn off initctl check for libvirtd
  * correct error "KVM version not showing in Ovirt Manager" when using qemu-rhev instead quemu

patches:
  * http://www.dreyou.org/ovirt/vdsm32.patch