## oVirt/VDSM CentOS repositories


**ovirt-dre.repo** - main repositories

  * vdsm-dre - vdsm rpms builded from latest git sources (__enabled__ by default)
  * ovirt-dre - oVirt rpms builded from latest git sources (__enabled__ by default)

  * vdsm31-dre - vdsm rpms for oVirt 3.1, builded from vdsm //ovirt-3.1// branch (disabled by default)
  * ovirt31-dre - oVirt rpms builded from oVirt //engine_3.1// branch (disabled by default)
 
  * vdsm32-dre - vdsm rpms for oVirt 3.2, builded from vdsm //ovirt-3.2// branch (disabled by default)
  * ovirt32-dre - oVirt rpms builded from oVirt //engine_3.2// branch (disabled by default)


To install:

````shell script
  sudo wget http://www.dreyou.org/ovirt/ovirt-dre.repo -P /etc/yum.repos.d/
````

**ovirt-tests-dre.repo** - repositories for untested rpms

  * vdsm-tests-dre - vdsm rpms builded from latest git sources (disabled by default)
  * ovirt-tests-dre - oVirt rpms builded from latest git sources (disabled by default)

  * vdsm31-tests-dre - vdsm rpms for oVirt 3.1, builded from vdsm _ovirt-3.1_ branch (disabled by default)
  * ovirt31-tests-dre - oVirt rpms builded from oVirt _engine_3.1_ branch (disabled by default)

  * vdsm32-tests-dre - vdsm rpms for oVirt 3.2, builded from vdsm _ovirt-3.2_ branch (disabled by default)
  * ovirt32-tests-dre - oVirt rpms builded from oVirt _engine_3.2_ branch (disabled by default)

To install:

````shell script
  sudo wget http://www.dreyou.org/ovirt/ovirt-tests-dre.repo -P /etc/yum.repos.d/
````

**addons-dre.repo** - additional rpms

  * addons - additional vdsm hooks (simpleqemu,snfilter)

## Miscellaneous files

  * Miscellaneous files, rpms, etc - http://www.dreyou.org/ovirt/misc/
