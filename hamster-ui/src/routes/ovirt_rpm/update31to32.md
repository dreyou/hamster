<script>
    import Note from '$lib/Note.svelte';
</script>
<Note warning="true" header="Warning">

Draft, not final. Be careful.

</Note>
&nbsp;
<Note>

I'm tested this procedure in my test lab with fresh oVirt 3.1 installation with following configuration:

  * oVirt 3.1 engine node (http://www.dreyou.org/ovirt/vdsm31/) , updated CentOS 6.4
  * 2 hosts with vdsm (http://www.dreyou.org/ovirt/vdsm31/), updated CentOS 6.4
  * standalone NFS data storage for Storage/ISO/Export domains
  * oVirt was installed and confugured with NFS storage and 2 hosts, 1 virtual server created and started

</Note>

## oVirt 3.1 to 3.2 Update Procedure

To update ovirt 3.1 to version 3.2 you can follow next procedure:

1. If possible, [backup engine](http://www.ovirt.org/Backup_And_Restore_Engine),
 also you can backup all VM to export domain using oVirt console.

2. Add info about 3.2 repo to /etc/yum.repos.d/ovirt-dre.repo and 

````ini
[vdsm32-dre]
name=Vdsm for oVirt 3.2 repo
baseurl=http://www.dreyou.org/ovirt/vdsm32/
http://www1.dreyou.org/ovirt/vdsm32/
enabled=1
gpgcheck=0

[ovirt32-dre]
name=oVirt 3.2 engine repo
baseurl=http://www.dreyou.org/ovirt/ovirt-engine32/
http://www1.dreyou.org/ovirt/ovirt-engine32/
enabled=1
gpgcheck=0
````

3. Check if 31 repo also ENABLED in /etc/yum.repos.d/ovirt-dre.repo

````ini
[vdsm31-dre]
name=Vdsm for oVirt 3.1 repo
baseurl=http://www.dreyou.org/ovirt/vdsm31/
http://www1.dreyou.org/ovirt/vdsm31/
enabled=1
gpgcheck=0
[ovirt31-dre]
name=oVirt 3.1 engine repo
baseurl=http://www.dreyou.org/ovirt/ovirt-engine31/
http://www1.dreyou.org/ovirt/ovirt-engine31/
enabled=1
gpgcheck=0
````

2. On oVirt engine node execute **engine-upgrade**

````shell script
engine-upgrade
````

3. If **engine-upgrade** found new version of **ovirt-engine-setup** you will be prompted to update this package, do it, then run **engine-upgrade** again:

````shell script
yum update ovirt-engine-setup
  
engine-upgrade
````
  
4. engine-upgrade will stop oVirt engine, then back up database, then update packages and then start oVirt engine again.

5. Check if engine works correctly, if yes, now you can start to upgrade hosts.

6. Switch host to "Maintenance" mode.

7. Add info about 3.2 repo to /etc/yum.repos.d/ovirt-dre.repo

````ini
[vdsm32-dre]
name=Vdsm for oVirt 3.2 repo
baseurl=http://www.dreyou.org/ovirt/vdsm32/
http://www1.dreyou.org/ovirt/vdsm32/
enabled=1
gpgcheck=0
````

8. Disable 3.1 repo in /etc/yum.repos.d/ovirt-dre.repo

````ini
[vdsm32-dre]
name=Vdsm for oVirt 3.1 repo
baseurl=http://www.dreyou.org/ovirt/vdsm31/
http://www1.dreyou.org/ovirt/vdsm31/
enabled=0
gpgcheck=0
````

9. Add epel repositories for __mom__ and for __python-pthreading__ packages (http://fedoraproject.org/wiki/EPEL/FAQ).

If you plan to use __gluster__ volumes, add gluster 3.4-alpha repositories (needed by vdsm-gluster)

````shell script
sudo wget http://download.gluster.org/pub/gluster/glusterfs/qa-releases/3.4.0alpha2/CentOS/glusterfs-alpha-epel.repo -P /etc/yum.repos.d/
````

10. Update **vdsm** packages.

````shell script
yum clean all&&yum makecache
yum update
````  

11. Restart **vdsmd** and activate host.

````shell script
service vdsmd restart
````