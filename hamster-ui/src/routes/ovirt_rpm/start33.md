## oVirt Engine 3.3.0 for CentOs/Scientific Linux 6.x Update HowTo


**HOWTO** update from my oVirt Engine **3.2.x** packages to official el6 **3.3.0** stable packages.

Before starting upgrade procedure, if possible, [backup engine](http://www.ovirt.org/),
 also you can backup all VM to export domain using oVirt console.


### oVirt Engine Update

1. Stop oVirt engine services

````shell script
service ovirt-engine stop
service ovirt-engine-dwhd stop
````

2. Remove local jbossas711 package

````shell script
rpm -e --nodeps ovirt-engine-jbossas711
````

3. Remove oVirt reports packages

````shell script
yum remove ovirt-engine-reports
yum remove jasperreports-server-4.7.0-1
yum remove ovirt-engine-dwh
````

4. Install oVirt el6 repository

````shell script
yum localinstall http://ovirt.org/releases/ovirt-release-el.noarch.rpm
````

5. Install jboss-as package from oVirt el6 repository

````shell script
yum install jboss-as-7.1.1
````

6. Run engine-upgrade they must found new version of ovirt-engine-setup and
 you will be prompted to update this package

````shell script
engine-upgrade
yum update ovirt-engine-setup
````

7. Possible correction of DB upgrade scripts

````shell script
cd /usr/share/ovirt-engine/dbscripts/upgrade
ls -1 03_02_0340*
03_02_0340_add_update_column_to_statistics_tables.sql
03_02_0340_gluster_volume_status_events.sql
````

If you look 2 files, rename one of them, otherwise do nothing

````shell script
mv 03_02_0340_gluster_volume_status_events.sql 03_02_0341_gluster_volume_status_events.sql
````

8. Run engine-setup to upgrade current installation

````shell script
engine-setup 
````
  
9. Disable or remove unnecessary yum repositories

````shell script
mv /etc/yum.repos.d/ovirt-dre.repo /etc/yum.repos.d/ovirt-dre.repo.save
````

### Hosts update
  
Connect to each host and update vdsm packages

1. Install oVirt el6 repository

````shell script
yum localinstall http://ovirt.org/releases/ovirt-release-el.noarch.rpm
````
  
2. Stop vdsmd daemon

````shell script
service vdsmd stop
````
  
3. Check if you has old glusterfs-epel.repo

````shell script
vim /etc/yum.repos.d/glusterfs-epel.repo
````

If you has this file and you have baseurl string pointed to 3.4.0 version, [correct it to 3.4.1](https://bugzilla.redhat.com/show_bug.cgi?id=1033587),
 here is a short command to do it

````shell script
sed -i 's/3.4.0/3.4.1/' /etc/yum.repos.d/glusterfs-epel.repo
````

4. Update vdsm

````shell script
yum update vdsm* mom
````
  
5. Check you /etc/vdsm/vdsm.conf and add necessary changes

````shell script
vim /etc/vdsm/vdsm.conf
````

6. Stop libvirtd daemon

````shell script
service libvirtd stop
````
  
7. Start vdsmd daemon (vdsmd will start libvirtd again)

````shell script
service vdsmd start
````

If you see message like this: //libvir: XML-RPC error : Failed to connect socket to '/var/run/libvirt/libvirt-sock': No such file or directory//, stop vdsmd and libvirtd and start it again

````shell script
service vdsmd stop
service libvirtd stop
service vdsmd start
````

 
8. Disable or remove unnecessary yum repositories

````shell script
mv /etc/yum.repos.d/ovirt-dre.repo /etc/yum.repos.d/ovirt-dre.repo.save
````
  
9. After update to CentOS 6.5, you can replace, if present, **qemu-img-rhev** packages

````shell script
service vdsmd stop
service libvirtd stop
````

or

````shell script
initctl stop libvirtd

rpm -e --nodeps qemu-img-rhev
rpm -e --nodeps qemu-kvm-rhev
rpm -e --nodeps qemu-kvm-rhev-tools
yum install qemu-img qemu-kvm qemu-kvm-tools

service vdsmd start
````

### Additional information

You can find more information about oVirt 3.3 at [oVirt web site](http://www.ovirt.org)