## Build information

**Dec 11 2012** - added qemu-rhev and sanlock 2.3-4 packages builded from redhat srpms

**Mar 11 2013** - latest build (buid-17)


### Vdsm notes:

v 4.10.0-0.46 (.17), builded from b59c8430b2a511bcea3bc1a954eee4ca1c0f4861 (branch ovirt-3.1)

Changes from original packages:

  * added softdog module autoloading
  * added local build number to rpm release info
  * turn off initctl check for libvirtd
  * move my nonstandart hooks (simpleqemu,snfilter) to separate repo
  * correct error "KVM version not showing in Ovirt Manager" when using qemu-rhev instead quemu
  * added patches
    * http://gerrit.ovirt.org/#/c/12723/
    * http://gerrit.ovirt.org/#/c/12836/

patches:
  * http://www.dreyou.org/ovirt/vdsm31.patch