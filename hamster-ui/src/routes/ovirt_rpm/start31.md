## oVirt Engine 3.1 Stable

[oVirt 3.1 Release Notes](http://wiki.ovirt.org/wiki/OVirt_3.1_release_notes)

Version 3.1-stable builds

  * qemu-rhev, sanlock **Dec 11 2012** ([more info](/ovirt_rpm/buildsvdsm31))
  * Vdsm **Mar 11 2013** (buid-17) ([more info](/ovirt_rpm/buildsvdsm31)) &#8252;[How to update to new build/vesion](/ovirt_rpm/vdsm31update)
  * oVirt **Aug 15 2012** (buid-19) ([more info](/ovirt_rpm/builds31)) &#8252;[How to update to new build/vesion](/ovirt_rpm/ovirt31update)

### Installation tips

#### Adding repo to hosts and management server

````shell script
sudo wget http://www.dreyou.org/ovirt/ovirt-dre.repo -P /etc/yum.repos.d/
````

Chek if **ovirt31-dre**, **vdsm31-dre** repositories enabled by default in /etc/yum.repos.d/ovirt-dre.repo
 (_enable=1_).

````shell script
yum clean all
yum makecache
````

### Node preparation

Check that the engine FQDN is resolvable from the node (For example, edit /etc/hosts).

Check that the CentOS version is 6.3, make update if needed.

````shell script
yum update
````

### Install ovirt-engine

#### Install ovirt engine

Add epel repositories for python-ply, python-kitchen needed by ovirt-engine-cli (http://fedoraproject.org/wiki/EPEL/FAQ).

Install python-ply and python-kitchen

````shell script
yum install python-ply python-kitchen
```` 

Install  ovirt-engine and ovirt-engine-cli, but disable epel repositories, which contains other ovirt-engine-cli  and ovirt-engine-sdk packages


````shell script
yum --disablerepo=epel install ovirt-engine ovirt-engine-cli
````

### Configure ovirt engine==

````shell script
engine-setup
````
  
Now you can enter to engine console and add you node to cluster.

### Troubleshooting==

If you have some problems while installing engine or running Vms look [here](/ovirt_rpm/troubleshooting).