import preprocess from 'svelte-preprocess';
import node from '@sveltejs/adapter-node';
import stat from '@sveltejs/adapter-static';


import { mdsvex } from 'mdsvex'

/** @type {import('@sveltejs/kit').Config} */
const config = {
//	preprocess: preprocess(),
	extensions: ['.svelte', '.md'],
	preprocess: [
        mdsvex({extension: 'md'})
	],
	kit: {
		// By default, `npm run build` will create a standard Node app.
		// You can create optimized builds for different platforms by
		// specifying a different adapter
		adapter: stat(),

		hydrate: true,
		router: true,

		// hydrate the <div id="svelte"> element in src/app.html
		target: '#svelte',
	}
};

export default config;