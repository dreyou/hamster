const { expect } = require('chai');

const TEST_URL = process.env.TEST_URL || "http://localhost:3000";

describe('Pages tests', function () {
  let page;

  before (async function () {
    page = await browser.newPage();
    await page.setViewport({
        width: 1024,
        height: 768,
        deviceScaleFactor: 1,
    });
    await page.goto(TEST_URL);
  });

  after (async function () {
    await page.close();
  })

  it('main should have the correct page title', async function () {
    expect(await page.title()).to.eql('dreyou home page');
  });

  it('main page should have a heading', async function () {
    const HEADING_SELECTOR = 'h2';
    let heading;

    await page.waitFor(HEADING_SELECTOR);
    heading = await page.$eval(HEADING_SELECTOR, heading => heading.innerText);

    expect(heading).to.eql('dreyou personal page');
  });

  it('page should have a single header section', async function () {
    const HEADER_SELECTOR = '.header';

    await page.waitFor(HEADER_SELECTOR);

    expect(await page.$$(HEADER_SELECTOR)).to.have.lengthOf(1);
  });

  it('check Svelte/Kit page navigation', async function () {
    await page.click('a[href="/tools/hamster"]', {waitForNavigationUntil: 'load'});

    await page.waitFor('.note_container');

    await page.waitFor('.box > h2');

    const h2 = await page.$eval('.box > h2', h2 => h2.innerText);
    expect(h2).to.eql('Hamster - Sample Svelte/Kit project');
  });

  it('check repository page navigation (thru Home)', async function () {
    await page.click('a[href="/"]', {waitForNavigationUntil: 'load'});

    await page.click('a[href="/tools/repo"]', {waitForNavigationUntil: 'load'});

    await page.waitFor('pre > code');

    await page.waitFor('.language-bash');

    await page.waitFor('.box > h2');

    const h2 = await page.$eval('.box > h2', h2 => h2.innerText);
    expect(h2).to.eql('Local repository installation');
  });

  it('check spwd page navigation (thru Home)', async function () {
    await page.click('a[href="/"]', {waitForNavigationUntil: 'load'});

    await page.click('a[href="/tools/spwd"]', {waitForNavigationUntil: 'load'});

    await page.waitFor('pre > code');

    await page.waitFor('.language-bash');

    await page.waitFor('.language-ini');

    await page.waitFor('.box > h2');

    const h2 = await page.$eval('.box > h2', h2 => h2.innerText);
    expect(h2).to.eql('spwd - Sample golang /proc to Web Daemon');
  });

  it('check archive page navigation', async function () {
    await page.click('a[href="/archive"].button', {waitForNavigationUntil: 'load'});

    await page.waitFor('ul > li');

    await page.waitFor('ul > li > a');

    const links = await page.$$eval('ul > li > a', links =>
      links.map(link => link.textContent)
    )
    expect(links).to.have.lengthOf(13)
    expect(links[0]).to.be.a('string')
    expect(links[0]).to.equal('oVirt 3.2 -> 3.3.0 Update HOW-TO')
    expect(links[10]).to.equal('Simple nwfilter')

    await page.waitFor('.box > h3');

    const h2 = await page.$eval('.box > h3', h2 => h2.innerText);
    expect(h2).to.eql('Ovirt/Vdsm packaging');
  });

  it('check samplepdf page navigation (thru Home)', async function () {
    await page.click('a[href="/"]', {waitForNavigationUntil: 'load'});

    await page.click('a[href="/tools/samplepdf"]', {waitForNavigationUntil: 'load'});

    await page.waitFor('.box > .note_container');

    await page.waitFor('.box > h2');

    const h2 = await page.$eval('.box > h2', h2 => h2.innerText);
    expect(h2).to.eql('samplepdf - Sample spring-boot XML to FO to PDF project');
  });

});